# Starting PostgreSQL

Copy the `docker-compose.yml` file from this directory, and start the container:

```bash
kea-db:~$ docker-compose up
```

# Connecting From The Server

To connect to the database shell from the server (logged in via SSH):

```bash
kea-db:~$ psql --host=127.0.0.1 --port=5432 --username=pguser pgdb
```

# Connect From Your Local Machine

Create an SSH tunnel to the development server:

```bash
ssh -L 5432:kea-db:5432 kea-db
```

From a different terminal on your local machine:

```bash
psql --host=127.0.0.1 --port=5432 --username=pguser pgdb
```

